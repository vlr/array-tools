import { printVersion, versionHigher, getGitTagVersion, getNpmVersion, incrementVersion } from "./versions";

export async function createTag(): Promise<void> {
  const npm = await getNpmVersion();
  const git = await getGitTagVersion();
  const result = versionHigher(npm, git) ? npm : incrementVersion(git);
  const version = printVersion(result);
  console.log(version);
  // await setGitTag(version);
}
