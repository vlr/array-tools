import { expect } from "chai";
import { unique } from "../src";

describe("unique", function (): void {
  it("should return items unique by single field", function (): void {
    // arrange
    const input = [{ id: 1 }, { id: 2 }, { id: 1 }];
    const expected = [{ id: 1 }, { id: 2 }];

    // act
    const result = unique(input, item => item.id);

    // assert
    expect(result).to.be.deep.equal(expected);
  });

  it("should return items unique by field combination", function (): void {
    // arrange
    const input = [
      { id: 1, name: "1", last: "2" },
      { id: 1, name: "2", last: "2" },
      { id: 2, name: "1", last: "2" },
      { id: 3, name: "3", last: "3" },
      { id: 1, name: "2", last: "1" },
      { id: 2, name: "1", last: "4" }
    ];
    const expected = [
      { id: 1, name: "1", last: "2" },
      { id: 1, name: "2", last: "2" },
      { id: 2, name: "1", last: "2" },
      { id: 3, name: "3", last: "3" }
    ];

    // act
    const result = unique(input, item => item.id, item => item.name);

    // assert
    expect(result).to.be.deep.equal(expected);
  });

  it("should have linear performance", function (): void {
    // arrange
    const input = [];
    for (let a = 0; a < 500000; a++) {
      input.push({ id1: getRandomInt(1, 100), id2: getRandomInt(1, 100) });
    }

    // act
    const start = new Date();
    unique(input, item => item.id1, item => item.id2);
    const end = new Date();

    // assert
    const time = end.getTime() - start.getTime();
    expect(time < 2000).equals(true);
  });
});

function getRandomInt(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min)) + min;
}
