import { splitInGroups, splitByGroupsCount } from "../src";
import { expect } from "chai";

describe("splitInGroups", function (): void {
  it("should return items unique by single field", function (): void {
    // arrange
    const input = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
    const expected = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13]];

    // act
    const result = splitInGroups(input, 4);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should split into one group if count is low", function (): void {
    // arrange
    const input = [1];
    const expected = [[1]];

    // act
    const result = splitByGroupsCount(input, 2);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should split by groups count", function (): void {
    // arrange
    const input = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
    const expected = [[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13]];

    // act
    const result = splitByGroupsCount(input, 3);

    // assert
    expect(result).deep.equals(expected);
  });
});
