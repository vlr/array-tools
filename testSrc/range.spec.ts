import { expect } from "chai";
import { range } from "../src";

describe("range", function (): void {
  it("should return array of size, filled with numbers", function (): void {
    // arrange
    const expected = [0, 1, 2];

    // act
    const result = range(3);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should return array from start to stop", function (): void {
    // arrange
    const expected = [3, 4, 5, 6, 7, 8];

    // act
    const result = range(3, 9);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should revert start and stop", function (): void {
    // arrange
    const expected = [3, 4, 5, 6, 7, 8];

    // act
    const result = range(9, 3);

    // assert
    expect(result).deep.equals(expected);
  });
});
