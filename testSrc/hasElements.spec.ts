import { expect } from "chai";
import { hasElements } from "../src";

describe("hasElements", function (): void {
  it("should return false for empty array", function (): void {
    // arrange

    // act
    const result = hasElements([]);

    // assert
    expect(result).equals(false);
  });

  it("should return false for null", function (): void {
    // arrange

    // act
    const result = hasElements(null);

    // assert
    expect(result).equals(false);
  });

  it("should return true for non empty array", function (): void {
    // arrange

    // act
    const result = hasElements([1]);

    // assert
    expect(result).equals(true);
  });

});
