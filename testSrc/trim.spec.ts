import { expect } from "chai";
import { trim, trimOne } from "../src";

describe("trim", function (): void {
  it("should trim 2 out of 3", function (): void {
    // arrange
    const input = [1, 2, 3];
    const expected = [1];

    // act
    const result = trim(input, 2);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should trim 3 out of 2 to result in empty array", function (): void {
    // arrange
    const input = [1, 2];
    const expected = [];

    // act
    const result = trim(input, 3);

    // assert
    expect(result).deep.equals(expected);
  });


  it("should trim one", function (): void {
    // arrange
    const input = [1, 2];
    const expected = [1];

    // act
    const result = trimOne(input);

    // assert
    expect(result).deep.equals(expected);
  });
});
