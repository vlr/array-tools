import { expect } from "chai";
import { findIndex } from "../src";

describe("findIndex", function (): void {
  it("should return index of fist item satisfying the predicate", function (): void {
    // arrange
    const input = [1, 2, 3, 4];
    // act
    const result = findIndex(input, item => item > 2);

    // assert
    expect(result).equals(2);
  });

  it("should return -1 if nothing satisfies", function (): void {
    // arrange
    const input = [1, 2, 1, 2];
    // act
    const result = findIndex(input, item => item > 2);

    // assert
    expect(result).equals(-1);
  });
});
