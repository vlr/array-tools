export function findIndex<T>(array: T[], predicate: (item: T) => boolean): number {
  if (!Array.isArray(array)) { return -1; }

  for (let i = 0; i < array.length; i++) {
    if (predicate(array[i])) {
      return i;
    }
  }

  return -1;
}
