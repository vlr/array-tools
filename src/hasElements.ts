export function hasElements(array: any[]): boolean {
  return Array.isArray(array) && array.length > 0;
}
