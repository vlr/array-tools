import { all } from "./all";
import { last } from "./last-first";

export interface BySelector<T> { (t: T): any; }

export function unique<T>(arr: T[], ...by: BySelector<T>[]): T[] {
  if (!arr) { return null; }
  switch (by.length) {
    case 0: return uniqueItem(arr);
    case 1: return uniqueByOne(arr, by[0]);
    default: return uniqueBySeveral(arr, by);
  }
}

function uniqueItem<T>(arr: T[]): T[] {
  const set = new Set<T>(arr);
  return Array.from(set);
}

function uniqueByOne<T>(arr: T[], by: BySelector<T>): T[] {
  const set = new Set();
  return arr.filter(item => {
    const key = by(item);
    if (set.has(key)) {
      return false;
    }
    set.add(key);
    return true;
  });
}

function uniqueBySeveral<T>(arr: T[], by: BySelector<T>[]): T[] {
  const map = new Map();
  return arr.filter(item => {
    if (isSet(map, by, item)) {
      return false;
    }

    set(map, by, item);
    return true;
  });
}

function isSet<T>(map: Map<any, any>, by: BySelector<T>[], item: T): boolean {
  return all(by, sel => map = map.get(sel(item)));
}

function set<T>(map: Map<any, any>, by: BySelector<T>[], item: T): void {
  const lastSelector = last(by);
  for (const selector of by) {
    const key = selector(item);
    if (selector === lastSelector) {
      map.set(key, true);
      return;
    }

    let subMap = map.get(key);
    if (!subMap) {
      subMap = new Map();
      map.set(key, subMap);
    }

    map = subMap;
  }
}
