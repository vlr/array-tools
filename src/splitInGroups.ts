export function splitInGroups<T>(items: T[], by: number): T[][] {
  if (!items) { return []; }
  const result: T[][] = [];
  let i = 0;
  while (i < items.length) {
    const next = i + by;
    result.push(items.slice(i, next));
    i = next;
  }
  return result;
}

export const splitByGroupSize = splitInGroups;

export function splitByGroupsCount<T>(items: T[], count: number): T[][] {
  if (count < 1) { return []; }
  return splitByGroupSize(items, Math.ceil(items.length / count));
}


