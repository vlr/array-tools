export function contains<T>(array: T[], item: T): boolean {
  return !!array && array.indexOf(item) !== -1;
}
