export function flatMap<TIn>(input: TIn[][]): TIn[];
export function flatMap<TIn, TOut>(input: TIn[], selector: (item: TIn, index: number) => TOut[]): TOut[];
export function flatMap<TIn, TOut>(input: TIn[], selector?: (item: TIn, index: number) => TOut[]): TOut[] {
  if (!input) { return []; }
  if (!selector) { return flatten(<any>input); }
  return input.reduce((res, item, index) => {
    const itemResult = selector(item, index);
    if (Array.isArray(itemResult) && itemResult.length) {
      res.push(...itemResult);
    }
    return res;
  }, []);
}

export function flatten<TIn>(input: TIn[][]): TIn[] {
  return input.reduce((res, item) => {
    if (Array.isArray(item) && item.length) {
      res.push(...item);
    }
    return res;
  }, []);
}
